window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/';
	const alertPlaceHolder = document.getElementById('liveAlertPlaceholder');

	try {
		const response = await fetch(url);

		if (!response.ok){
			alertPlaceHolder.innerHTML += `<div class="alert alert-dark" role="alert">ERROR: UNABLE TO FETCH STATES</div>`;
		} else{
			const data = await response.json();
			const selectTag = document.getElementById('state');
			for (const state of data.states) {
				const option = document.createElement('option');
				option.value = state.abbreviation;
				option.innerHTML = state.name;
				selectTag.appendChild(option);
			}
		}

		const formTag = document.getElementById('create-location-form');
		formTag.addEventListener("submit", async (event) => {
			event.preventDefault();

			const formData = new FormData(formTag);
			const json = JSON.stringify(Object.fromEntries(formData));

			const locationUrl = 'http://localhost:8000/api/locations/';
			const fetchConfig = {
			method: "post",
			body: json,
			headers: {
				'Content-Type': 'application/json',
			},
			};
			const response = await fetch(locationUrl, fetchConfig);
			if (response.ok) {
				formTag.reset();
				const newLocation = await response.json();
				console.log(newLocation);
			}
		});
	} catch(e) {
		console.log(e);
    	alertPlaceHolder.innerHTML += `<div class="alert alert-dark alert-dismissable" role="alert"><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>  ${e}</div>`;
	}
});