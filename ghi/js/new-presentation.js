window.addEventListener('DOMContentLoaded', async () => {

	const url = 'http://localhost:8000/api/conferences/';
	const alertPlaceHolder = document.getElementById('liveAlertPlaceholder');

  	try {
		const response = await fetch(url);

		if (!response.ok){
			alertPlaceHolder.innerHTML += `<div class="alert alert-dark" role="alert">ERROR: UNABLE TO FETCH CONFERENCES</div>`;
		} else{
			const data = await response.json();
			const selectTag = document.getElementById('conference');
			for (const conference of data.conferences) {
				const option = document.createElement('option');
				option.value = conference.id;
				option.innerHTML = conference.name;
				selectTag.appendChild(option);
			}
		}

		const formTag = document.getElementById('create-presentation-form');
		formTag.addEventListener("submit", async (event) => {
			event.preventDefault();

			const formData = new FormData(formTag);
			const json = JSON.stringify(Object.fromEntries(formData));
			const presentationUrl = 'http://localhost:8000/api/conference//presentations';
			const fetchConfig = {
			method: "post",
			body: json,
			headers: {
				'Content-Type': 'application/json',
			},
			};
			const response = await fetch(presentationUrl, fetchConfig);
			if (response.ok) {
				formTag.reset();
				const newPresentation = await response.json();
				console.log(newPresentation);
			}
		});
	} catch(e){
		console.log(e);
		alertPlaceHolder.innerHTML += `<div class="alert alert-dark alert-dismissable" role="alert"><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>  ${e}</div>`;
  	}
});
