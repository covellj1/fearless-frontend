import React, { useEffect, useState } from 'react';

function PresentationForm() {
  const [conferences, setConferences] = useState([])

  //Notice that we can condense all formData
  //into one state object
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    company: '',
    title: '',
    synopsis: '',
    conference: '',
  })

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    console.log(formData)
    const data = {};
    data.presenter_name = formData.name;
    data.presenter_email = formData.email;
    data.company_name = formData.company;
    data.title = formData.title;
    data.synopsis = formData.synopsis;
    data.conference = formData.conference;

    const url = 'http://localhost:8000/api/conferences/'+formData.conference+'/presentations/';
    const fetchConfig = {
      method: "post",
      //Because we are using one formData state object,
      //we can now pass it directly into our request!
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
        console.log("OKEY DOKEY")
      //The single formData object
      //also allows for easier clearing of data
        setFormData({
            name: '',
            email: '',
            company: '',
            title: '',
            synopsis: '',
            conference: '',
        });
    }
  }

  //Notice that we can also replace multiple form change
  //eventListener functions with one
  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    //We can condense our form data event handling
    //into on function by using the input name to update it

    setFormData({
      //Previous form data is spread (i.e. copied) into our new state object
      ...formData,

      //On top of the that data, we add the currently engaged input key and value
      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Submit a new presentation</h1>
          <form onSubmit={handleSubmit} id="create-presentation-form">

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={formData.name}/>
              <label htmlFor="name">Name</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Presenter email" required type="email" name="email" id="email" className="form-control" value={formData.email}/>
              <label htmlFor="email">Presenter email</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Company name" required type="text" name="company" id="company" className="form-control" value={formData.company}/>
              <label htmlFor="company">Company name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Title" required type="text" name="title" id="title" className="form-control" value={formData.title}/>
              <label htmlFor="title">Title</label>
            </div>
            <div className="mb-3">
              <label htmlFor="synopsis">Synopsis</label>
              <textarea onChange={handleFormChange} className="form-control" id="synopsis" rows="3" name="synopsis" value={formData.synopsis}></textarea>
            </div>

            <div className="mb-3">
              <select onChange={handleFormChange} required name="conference" id="conference" className="form-select" value={formData.conference}>
                <option value="">Choose a conference</option>
                {conferences.map(conference => {
                  return (
                    <option key={conference.id} value={conference.id}>{conference.name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default PresentationForm;