window.addEventListener('DOMContentLoaded', async () => {

	const url = 'http://localhost:8000/api/locations/';
	const alertPlaceHolder = document.getElementById('liveAlertPlaceholder');

  	try {
		const response = await fetch(url);

		if (!response.ok){
			alertPlaceHolder.innerHTML += `<div class="alert alert-dark" role="alert">ERROR: UNABLE TO FETCH LOCATIONS</div>`;
		} else{
			const data = await response.json();
			const selectTag = document.getElementById('location');
			for (const location of data.locations) {
				const option = document.createElement('option');
				option.value = location.id;
				option.innerHTML = location.name;
				selectTag.appendChild(option);
			}
		}

		const formTag = document.getElementById('create-conference-form');
		formTag.addEventListener("submit", async (event) => {
			event.preventDefault();
			const formData = new FormData(formTag);
			const json = JSON.stringify(Object.fromEntries(formData));
			const conferenceUrl = 'http://localhost:8000/api/conferences/';
			const fetchConfig = {
			method: "post",
			body: json,
			headers: {
				'Content-Type': 'application/json',
			},
			};
			const response = await fetch(conferenceUrl, fetchConfig);
			if (response.ok) {
				formTag.reset();
				const newConference = await response.json();
				console.log(newConference);
			}
		});
	} catch(e){
		console.log(e);
		alertPlaceHolder.innerHTML += `<div class="alert alert-dark alert-dismissable" role="alert"><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>  ${e}</div>`;
  	}
});
