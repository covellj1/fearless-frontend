function createCard(name, description, pictureUrl, dates, location) {
  return `
    <div class="card shadow-lg p-3 mb-5 bg-body-tertiary rounded">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class='card-subtitle mb-2 text-muted'>${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
          <p>${dates}</p>
      </div>
    </div>
  `;
}


function createPlaceholderCard() {
  return `
    <div class="card shadow-lg p-3 mb-5 bg-body-tertiary rounded" aria-hidden="true" id='placeholder'>
      <img class="card-img-top">
      <div class="card-body">
        <h5 class="card-title placeholder-glow"></h5>
        <h6 class='card-subtitle mb-2 text-muted placeholder-glow'></h6>
        <p class="card-text placeholder-glow">
          <span class="placeholder col-7"></span>
          <span class="placeholder col-4"></span>
          <span class="placeholder col-4"></span>
          <span class="placeholder col-6"></span>
          <span class="placeholder col-8"></span>
        </p>
      </div>
      <div class="card-footer">
          <p class="placeholder-glow"></p>
      </div>
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';
  const alertPlaceHolder = document.getElementById('liveAlertPlaceholder');
  try {

    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
      alertPlaceHolder.innerHTML += `<div class="alert alert-dark" role="alert">ERROR: UNABLE TO FETCH CONFERENCES</div>`;
  } else {
      const data = await response.json();


      let col_index = 0;
      for (let conference of data.conferences) {
        const columns = document.querySelectorAll('.col');
          const column = columns[col_index];
          column.innerHTML = createPlaceholderCard();
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {

          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;

          const start_date = new Date(details.conference.starts);
          const end_date = new Date(details.conference.ends);
          const dates = `${start_date.getMonth()}/${start_date.getDate()}/${start_date.getFullYear()} - ${end_date.getMonth()}/${end_date.getDate()}/${end_date.getFullYear()}`;
          const location = details.conference.location.name;
          const html = createCard(name, description, pictureUrl, dates, location);

          column.innerHTML = html;
          col_index++;
          if (col_index === 6){
            break
          }
        }
      }
    }
  } catch (e) {
    console.log(e);
    alertPlaceHolder.innerHTML += `<div class="alert alert-dark alert-dismissable" role="alert"><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>  ${e}</div>`;
    // Figure out what to do if an error is raised
  }

});